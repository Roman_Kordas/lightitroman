import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductCardComponent } from '../product-card/product-card.component';
import { ReviewComponent } from '../review/review.component';
import { LeftReviewComponent } from '../left-review/left-review.component';
import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { RegisterLoginComponent } from '../register-login/register-login.component';
import { CatalogComponent } from '../catalog/catalog.component';

const routes: Routes = [
  { path: 'catalog', component: CatalogComponent },
  { path: 'detail/:id', component: ProductDetailComponent },
  { path: 'login', component: RegisterLoginComponent },
  
  { path: '', redirectTo: 'catalog', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
