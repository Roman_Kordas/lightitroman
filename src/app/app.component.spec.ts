import { TestBed, async, ComponentFixture } from '@angular/core/testing';

import { By } from '@angular/platform-browser'
import { DebugElement } from '@angular/core'
import { RouterOutlet } from '@angular/router'

import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing'

import { MaterialComponentsModule } from './material-components/material-components.module';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialComponentsModule,
        RouterTestingModule.withRoutes([])
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent)
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should have a router outlet', () => {
    let de = fixture.debugElement.query(By.directive(RouterOutlet))

    expect(de).not.toBeNull();
  })
});
