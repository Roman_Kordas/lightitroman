import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MaterialComponentsModule } from './material-components/material-components.module';
import { AppRoutingModule } from './app-routing/app-routing.module';

import { HttpService } from './common/services/http.service';
import { ToastMessagesService } from './common/services/toast-messages.service';
import { AuthService } from './common/services/auth/auth.service';

import { AppComponent } from './app.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { ReviewComponent } from './review/review.component';
import { LeftReviewComponent } from './left-review/left-review.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { RegisterLoginComponent } from './register-login/register-login.component';
import { CatalogComponent } from './catalog/catalog.component';
import { StarRatingComponent } from './star-rating/star-rating.component';




@NgModule({
  declarations: [
    AppComponent,
    ProductCardComponent,
    ReviewComponent,
    LeftReviewComponent,
    ProductDetailComponent,
    RegisterLoginComponent,
    CatalogComponent,
    StarRatingComponent
  ],
  imports: [
    BrowserModule,                                                                                                                                                                                                      
    MaterialComponentsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
  ],
  providers: [
    HttpService,
    ToastMessagesService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
