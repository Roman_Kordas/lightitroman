import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebugElement } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { CatalogComponent } from './catalog.component';
import { MaterialComponentsModule } from '../material-components/material-components.module';
import { HttpService } from '../common/services/http.service';
import { ToastMessagesService } from '../common/services/toast-messages.service';
import { IProduct } from '../common/models/product.model';
import { ProductCardComponent } from '../product-card/product-card.component';
import { RouterTestingModule } from '@angular/router/testing';


class HttpServiceStub{
  getProducts(){
    return new Observable<IProduct[]>(observer => observer.next([
    {
      "id": 1,
      "title": "Product1",
      "img": "img1.png",
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam, placeat animi suscipit quas non odio harum quis, labore consectetur id repudiandae incidunt distinctio perspiciatis quisquam iste, neque eum amet. Voluptatum?"
    },
    {
      "id": 2,
      "title": "Product2",
      "img": "img2.png",
      "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam, placeat animi suscipit quas non odio harum quis, labore consectetur id repudiandae incidunt distinctio perspiciatis quisquam iste, neque eum amet. Voluptatum?"
    }
  ]));
  }

  getStaticURL(){
  }
}

class ToastMessagesServiceStub{
  ShowToastMessage(message: string, action?: string){
  }
}

describe('CatalogComponent', () => {
  let component: CatalogComponent;
  let fixture: ComponentFixture<CatalogComponent>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialComponentsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [ CatalogComponent, ProductCardComponent ],
      providers: [
        { provide: HttpService, useClass: HttpServiceStub },
        { provide: ToastMessagesService, useClass: ToastMessagesServiceStub }

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogComponent);
    component = fixture.componentInstance;

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
