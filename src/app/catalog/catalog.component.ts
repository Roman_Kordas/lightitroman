import { Component, OnInit } from '@angular/core';
import { HttpService } from '../common/services/http.service';

import { IProduct } from '../common/models/product.model';
import { Observable } from 'rxjs/Observable';
import { ToastMessagesService } from '../common/services/toast-messages.service';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
  providers:[]
})
export class CatalogComponent implements OnInit {

  $products: Observable<string | IProduct[]>;
  
  constructor(
    private httpService: HttpService,
    private toast: ToastMessagesService
  ){ }

  ngOnInit() {
    this.$products = this.httpService.getProducts();
    this.$products.subscribe(response => {
      if(typeof(response) === 'string'){
        // An error catched during request, reponse is the error message
        this.toast.ShowToastMessage(response, 'Ok');
      }        
    });
    
  }

}
