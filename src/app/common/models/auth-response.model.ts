export interface IAuthResponse{
    "success": string,
    "token": string
}