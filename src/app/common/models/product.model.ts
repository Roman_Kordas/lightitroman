export interface IProduct{
    "id"?: number, 
    "title": string,
    "img": string,
    "text": string,
}