export interface IReview{
    "id"?: number 
    "product"?: number
    "rate": number,
    "text": string, 
    "created_by"?: {
        "id": number,
        "username": string,
        "first_name": string,
        "last_name": string,
        "email": string
    }, 
    "created_at"?: string
}