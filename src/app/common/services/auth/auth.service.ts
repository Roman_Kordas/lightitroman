import { Injectable } from '@angular/core';

@Injectable()

export class AuthService {
    
    constructor() {}

    public saveToken(token: string){
        localStorage.setItem('token', token); 
    }

    public getToken(): string{
        return localStorage.getItem('token');
    }

    // For dev purposes only!
    public resetToken(){
        localStorage.removeItem('token'); 
    }

    public isAuthenticated(): boolean {
      const token = localStorage.getItem('token');
      // If token exist return true
      // For real authentification expiration check should be provided
      return token ? true : false;
    }
}