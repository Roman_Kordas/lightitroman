import { TestBed, inject } from '@angular/core/testing';

import { HttpService } from './http.service';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { HttpHeaders} from '@angular/common/http';
import { AuthService } from './auth/auth.service';


class AuthServiceStub{
  getToken(): string{
    return 'fake-token';
  }
}

describe('HttpService', () => {
  let httpService: HttpService;
  let httpTestingController: HttpTestingController;
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        HttpService,
        {provide: AuthService, useClass: AuthServiceStub}
      ]
    });

    httpService = TestBed.get(HttpService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(httpService).toBeTruthy();
  });

  it('getStaticURL should return valid url', () => {
    const STATIC = 'http://smktesting.herokuapp.com/static';
    const ITEM = 'img.png'

    let out = httpService.getStaticURL(ITEM);

    expect(out).toEqual('http://smktesting.herokuapp.com/static/img.png');
  });
});
