import { Injectable } from '@angular/core';

import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import 'rxjs/add/operator/map';

import { IProduct } from '../models/product.model';
import { IReview } from '../models/review.model';
import { IUser } from '../models/user.model';
import { IAuthResponse } from '../models/auth-response.model'

import { AuthService } from './auth/auth.service';

const API = 'http://localhost:3000/api';
const STATIC = 'http://smktesting.herokuapp.com/static';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  }),
};

@Injectable()
export class HttpService {

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

/*============================================================================*\

    @@ GET

\*============================================================================*/

  getProducts(){
    let url = `${API}/products`;

    return this.http.get<IProduct[]>(url, { observe: 'response' })
      .map(res => [...res.body])
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  getProduct(id: number){
    let url = `${API}/products/${id}`;

    return this.http.get<IProduct>(url, { observe: 'response' })
    .map(res => { return {...res.body} })  
    .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  getReviewsForProduct(productID: number){
    let url = `${API}/reviews?product=${productID}&_sort=created_at&_order=desc`

    return this.http.get<IReview[]>(url, { observe: 'response' })
    .map( res => [...res.body])
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

/*============================================================================*\

    @@ POST

\*============================================================================*/

  registerUser(user: IUser){
    let url = `${API}/register`;
        
    return this.http.post<IAuthResponse>(url, user, httpOptions)
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  loginUser(user: IUser){
    let url = `${API}/login`;
        
    return this.http.post<IAuthResponse>(url, user, httpOptions)
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  postReview(review: IReview){
    let url = `${API}/reviews`;
    let token = this.getToken();
    
    //Token is undefined, stop and return the error
    if (!token) return new Observable<string>(observer => observer.next('No token, need autorization'));
    let options = this.getHeadersWithToken(token);
    
    return this.http.post<IReview>(url, review, options)
    .pipe(
      retry(3),
      catchError(this.handleError)
    )
  }
/*============================================================================*\

    @@ CONTROLLERS

\*============================================================================*/

  getStaticURL(item: string): string{
    return `${STATIC}/${item}`;
  }

  getToken(): string{
    let token = this.authService.getToken();

    if(!token) throw new Error('Authentification failed, please login to the system.');

    return token;
  }

  getHeadersWithToken(token: string){    
    return {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'token': token
      })
    };
  }

/*============================================================================*\

    @@ ERROR HANDLING

\*============================================================================*/

  private handleError(error: HttpErrorResponse) {
    let message: string = '';

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.    
      message = 'Something went wrong on your side. Please try again later';

      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      message = 'Something went wrong on server side. Please try again later';

      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new Observable<string>(observer => observer.next(message));
  };

}