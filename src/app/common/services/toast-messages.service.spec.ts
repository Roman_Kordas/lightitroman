import { TestBed, inject } from '@angular/core/testing';

import { ToastMessagesService } from './toast-messages.service';
import { MaterialComponentsModule } from '../../material-components/material-components.module';

describe('ToastMessagesService', () => {
  let toastService: ToastMessagesService;

  beforeEach(() => {  
    TestBed.configureTestingModule({
      imports: [MaterialComponentsModule],
      providers: [ToastMessagesService]
    });

    toastService = TestBed.get(ToastMessagesService);
  });

  it('should be created',() => {
    expect(toastService).toBeTruthy();
  });
});
