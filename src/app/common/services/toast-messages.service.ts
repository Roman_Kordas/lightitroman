import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class ToastMessagesService {

  constructor(public snackBar: MatSnackBar) { }
  
  ShowToastMessage(message: string, action?: string){
    this.snackBar.open(message , action, {
        duration: 5000
    });
}
}
