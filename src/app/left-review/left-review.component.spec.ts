import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftReviewComponent } from './left-review.component';
import { Observable } from 'rxjs/Observable';

import { HttpService } from '../common/services/http.service';
import { AuthService } from '../common/services/auth/auth.service';
import { ToastMessagesService } from '../common/services/toast-messages.service';

import { IReview } from '../common/models/review.model';
import { MaterialComponentsModule } from '../material-components/material-components.module';
import { StarRatingComponent } from '../star-rating/star-rating.component';
import { FormsModule } from '@angular/forms';


class HttpServiceStub{
  postReview(review: IReview): Observable<IReview>{
    return new Observable(observer => observer => observer.next(
      {
        "id": 6,
        "product": 2,
        "rate": 4,
        "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam, placeat animi suscipit quas non odio harum quis, labore consectetur id repudiandae incidunt distinctio perspiciatis quisquam iste, neque eum amet. Voluptatum?",
        "created_by": {
          "id": 1,
          "username": "Tyson",
          "first_name": "",
          "last_name": "",
          "email": "user@user.com"
        },
        "created_at": "2013-03-19T00:00:00Z"
      }
    ))
  }
} 

class AuthServiceStub{
  isAuthenticated(): boolean{
    return true;
  }
}

class ToastMessagesServiceStub{
  ShowToastMessage(message: string, action?: string){
  }
}

describe('LeftReviewComponent', () => {
  let component: LeftReviewComponent;
  let fixture: ComponentFixture<LeftReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialComponentsModule,
        FormsModule
      ],
      declarations: [ LeftReviewComponent, StarRatingComponent ],
      providers: [
        { provide: HttpService, useClass: HttpServiceStub },
        { provide: AuthService, useClass: AuthServiceStub },
        { provide: ToastMessagesService, useClass: ToastMessagesServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
