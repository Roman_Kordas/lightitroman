import { Component, OnInit, Input } from '@angular/core';

import { HttpService } from '../common/services/http.service';
import { AuthService } from '../common/services/auth/auth.service';
import { ToastMessagesService } from '../common/services/toast-messages.service';

@Component({
  selector: 'app-left-review',
  templateUrl: './left-review.component.html',
  styleUrls: ['./left-review.component.scss']
})

export class LeftReviewComponent implements OnInit {
  
  private _reviewText: string;
  private _rating: number;
  private _starCount: number = 5;
  private _starColor: string = 'primary';
  private _isAuthentificated: boolean = true;


  constructor(
    private httpService: HttpService,
    private authService: AuthService,
    private toast: ToastMessagesService
  ){}

  ngOnInit() { }

  onSubmit(){

    let review = {
      'rate': this._rating,
      'text': this._reviewText
    }
    
    this._isAuthentificated = this.authService.isAuthenticated();

    if(this._isAuthentificated && this._rating){
      this.httpService.postReview(review).subscribe(response => {
        if(response instanceof Object){
          this.toast.ShowToastMessage('Your review was saved ', 'Ok');
        }else{
          // An error catched during request, reponse is the error message
          this.toast.ShowToastMessage(response, 'Ok');
        }        
      });
    }

    if(!this._rating){
      this.toast.ShowToastMessage('Rate your experience (set some stars) befor posting your review', 'Ok');
    }
  }

  onRatingChanged(rating){
    this._rating = rating;
  }
}
