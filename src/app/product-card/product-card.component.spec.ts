import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCardComponent } from './product-card.component';
import { MaterialComponentsModule } from '../material-components/material-components.module';

import { HttpService } from '../common/services/http.service';
import { Observable } from 'rxjs/Observable';

class HttpServiceStub{
  getStaticURL(): string{
    return 'http://fakeurl.com';
  }
}

describe('ProductCardComponent', () => {
  let component: ProductCardComponent;
  let fixture: ComponentFixture<ProductCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialComponentsModule
      ],
      declarations: [ ProductCardComponent ],
      providers: [
        { provide: HttpService, useClass: HttpServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
