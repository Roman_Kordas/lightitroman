import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';

import { HttpService } from '../common/services/http.service';

import { IProduct } from '../common/models/product.model';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit, OnChanges {

  
  @Input() product: IProduct;
  imgUrl: string;

  constructor(private httpService: HttpService) { }

  ngOnChanges(changes: SimpleChanges): void {    
    if(changes.product.currentValue.id){
      this.imgUrl = this.httpService.getStaticURL(this.product.img);
    }
  }
  
  ngOnInit() {
  }

  

}
