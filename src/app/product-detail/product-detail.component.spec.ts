import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { ProductDetailComponent } from './product-detail.component';
import { MaterialComponentsModule } from '../material-components/material-components.module';
import { ToastMessagesService } from '../common/services/toast-messages.service';
import { HttpService } from '../common/services/http.service';
import { RouterTestingModule } from '@angular/router/testing'
import { IProduct } from '../common/models/product.model'
import { IReview } from '../common/models/review.model';
import { LeftReviewComponent } from '../left-review/left-review.component';
import { ReviewComponent } from '../review/review.component';
import { StarRatingComponent } from '../star-rating/star-rating.component';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../common/services/auth/auth.service';


class ToastMessagesServiceStub{
  ShowToastMessage(message: string, action?: string){
  }
}

class HttpServiceStub{
  getReviewsForProduct(id: number){
    return new Observable<IReview[]>(observer => observer.next([
      {
        "id": 1,
        "product": 1,
        "rate": 2,
        "text": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam, placeat animi suscipit quas non odio harum quis, labore consectetur id repudiandae incidunt distinctio perspiciatis quisquam iste, neque eum amet. Voluptatum?",
        "created_by": {
          "id": 1,
          "username": "Sophia",
          "first_name": "",
          "last_name": "",
          "email": "user@user.com"
        },
        "created_at": "2013-12-09T00:00:00Z"
      }
    ]));
  }

  getStaticURL(){
  }
}

class AuthServiceStub{
  isAuthenticated(): boolean{
    return true;
  }
}


describe('ProductDetailComponent', () => {
  let component: ProductDetailComponent;
  let fixture: ComponentFixture<ProductDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialComponentsModule,
        FormsModule,
        RouterTestingModule
      ],
      declarations: [
        ProductDetailComponent,
        LeftReviewComponent,
        ReviewComponent,
        StarRatingComponent
      ],
      providers: [
        { provide: HttpService, useClass:  HttpServiceStub },
        { provide: ToastMessagesService,  useClass: ToastMessagesServiceStub },
        { provide: AuthService, useClass: AuthServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
