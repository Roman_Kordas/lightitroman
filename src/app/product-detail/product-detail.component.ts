import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HttpService } from '../common/services/http.service';
import { Observable } from 'rxjs/Observable';

import { IProduct } from '../common/models/product.model'
import { IReview } from '../common/models/review.model';
import { ToastMessagesService } from '../common/services/toast-messages.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: IProduct;
  imgUrl:string = 'http://via.placeholder.com/350x350';

  $reviews: Observable<string | IReview[]>;
  
  constructor(
    private route: ActivatedRoute,
    private httpService: HttpService,
    private toast: ToastMessagesService
  ) { }

  getProduct(productID: number){ 
    this.httpService.getProduct(productID).subscribe( response => {  
      if(response instanceof Object){
        this.product = { ...response as IProduct };      
        this.imgUrl = this.httpService.getStaticURL(this.product.img);
      }else{
        // An error catched during request, reponse is the error message
        this.toast.ShowToastMessage(response, 'Ok');
      }        
    })
  }

  ngOnInit() {
    // parsing product id from the URL
    const id = +this.route.snapshot.paramMap.get('id');
    
    this.getProduct(id);
    this.$reviews =  this.httpService.getReviewsForProduct(id);
    this.$reviews.subscribe(response => {
      if(typeof(response) === 'string'){
        // An error catched during request, reponse is the error message
        this.toast.ShowToastMessage(response, 'Ok');
      }        
    });
  }

}
