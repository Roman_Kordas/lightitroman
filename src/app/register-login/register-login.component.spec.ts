import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { RegisterLoginComponent } from './register-login.component';
import { ToastMessagesService } from '../common/services/toast-messages.service';
import { HttpService } from '../common/services/http.service';
import { AuthService } from '../common/services/auth/auth.service';
import { IAuthResponse } from '../common/models/auth-response.model'
import { MaterialComponentsModule } from '../material-components/material-components.module';
import { FormsModule } from '@angular/forms';

class ToastMessagesServiceStub{
  ShowToastMessage(message: string, action?: string){
  }
}

class HttpServiceStub{

  registerUser(){
    return new Observable<IAuthResponse>(observer => observer.next({
      "success": 'true',
      "token": 'fake-token'
    }));
  }
  

  loginUser(){
    return new Observable<IAuthResponse>(observer => observer.next({
      "success": 'true',
      "token": 'fake-token'
    }));
  }  
  
  getStaticURL(){
  }
}

class AuthServiceStub{
  isAuthenticated(): boolean{
    return true;
  }
}

describe('RegisterLoginComponent', () => {
  let component: RegisterLoginComponent;
  let fixture: ComponentFixture<RegisterLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialComponentsModule,
        FormsModule
      ],
      declarations: [ RegisterLoginComponent ],
      providers:[
        { provide: HttpService, useClass:  HttpServiceStub },
        { provide: ToastMessagesService,  useClass: ToastMessagesServiceStub },
        { provide: AuthService, useClass: AuthServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
