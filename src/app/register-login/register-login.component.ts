import { Component, OnInit } from '@angular/core';

import { HttpService } from '../common/services/http.service';
import { AuthService } from '../common/services/auth/auth.service';
import { ToastMessagesService } from '../common/services/toast-messages.service';

import { IUser } from '../common/models/user.model'
import { IAuthResponse } from '../common/models/auth-response.model'
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-register-login',
  templateUrl: './register-login.component.html',
  styleUrls: ['./register-login.component.scss']
})

export class RegisterLoginComponent implements OnInit {

  private _userName: string;
  private _password: string;
  hasAccount: boolean = true;

  constructor(
    private httpService: HttpService,
    private authService: AuthService,
    private toast: ToastMessagesService
  ) { }

  ngOnInit() { }

  onSubmit(){
    let user = {
      "username": this._userName,
      "password": this._password
    }

    this.hasAccount 
    ? this.login(user)
    : this.register(user)
  }

  register(user: IUser){
    this.httpService.registerUser(user).subscribe(resp => {
      this.checkBeforeSaving(resp, 'Registration');
    });
  }

  login(user: IUser){
    this.httpService.loginUser(user).subscribe(resp => {
      this.checkBeforeSaving(resp, 'Autorization');
    });
  }

  checkBeforeSaving(response, action: string){
    if(response instanceof Object){
      if(response.success == 'true'){
        this.saveToken(response.token);
        this.showMessage(`${action} completed successfully!`, 'Ok');
      }else{
        this.showMessage(`${action} failed. Server decline your request`);
      }
    }else{
      // An error catched during request, reponse is the error message
      this.showMessage(response);
    }
  }

  saveToken(token: string){
    this.authService.saveToken(token);
  }

  showMessage(message: string, option?: string){
    this.toast.ShowToastMessage(message, option);
  }

}
