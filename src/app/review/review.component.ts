import { Component, OnInit, Input } from '@angular/core';
import { IReview } from '../common/models/review.model';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})

export class ReviewComponent implements OnInit {

  starCount: number = 5;
  starColor: string = 'primary';

  @Input() review;
  
  constructor() { }
    
  ngOnInit() { }
 
}
