module.exports = (req, res, next) => {
    // Stub for login & registor action
    if(req.originalUrl.includes('login') || req.originalUrl.includes('register')){
        if (req.method === 'POST') {
            // Converts POST to GET and move payload to query params
            // This way it will make JSON Server that it's GET request
            req.method = 'GET';
            req.query = req.body;
        }
    }
    next()
}