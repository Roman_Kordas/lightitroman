module.exports = (req, res, next) => {
    // Stub for posting review
    if(req.originalUrl.includes('reviews')){
        if (req.method === 'POST') {
            
            // Parse product ID
            let referer = req.headers.referer;
            let productID = referer.slice(referer.lastIndexOf('/') + 1);
                      
            let payload = {
                "product": productID,
                
                "created_by": {
                  "username": "user",
                  "first_name": "",
                  "last_name": "",
                  "email": "user@user.com"
                },
                "created_at": new Date().toISOString()
            }

            req.body = {...req.body, ...payload}
            console.log(req.body);
        }
    }
    next()
}